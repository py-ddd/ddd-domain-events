import unittest
from enum import Enum
from typing import List
from unittest.mock import patch, MagicMock

from ddd_domain_events import DomainEvents, DomainEventCallable
from ddd_domain_events.domain_events import MissingDomainEventsContext


class OrderEvent(Enum):
    """Domain Event raised for special order use cases"""
    HIGH_QUANTITY = 'HIGH_QUANTITY'
    HIGH_VOLUME_PRICE = 'HIGH_VOLUME_PRICE'


class OrderItem:
    """OrderItem value object that contains order details for a single item"""
    def __init__(self, product_id: str, price: float, quantity: int):
        self.product_id = product_id
        self.price = price
        self.quantity = quantity


class Order:
    """Order entity that contains order items"""
    HIGH_VOLUME_PRICE = 1_000_000
    HIGH_QUANTITY = 10_000

    def __init__(self):
        self._order_items = []

    @property
    def order_items(self):
        for order_item in self._order_items:
            yield order_item

    def add_order_items(self, order_items: List[OrderItem]) -> None:
        total_price = 0
        total_quantity = 0

        for order_item in order_items:
            total_price += (order_item.price * order_item.quantity)
            total_quantity += order_item.quantity
            # Process the actual business logic related to this method,
            # which is add OrderItem value objects to this Order Entity
            self._order_items.append(order_item)

        # Notify whoever might be interested about high price volume orders
        if total_price >= self.HIGH_VOLUME_PRICE:
            DomainEvents.raise_event(OrderEvent.HIGH_VOLUME_PRICE, order=self)

        # Notify whoever might be interested about high quantity volume orders
        if total_quantity >= self.HIGH_QUANTITY:
            DomainEvents.raise_event(OrderEvent.HIGH_QUANTITY, order=self)


class OrderService:
    """Application Service for handling Order related operations"""
    @classmethod
    def create_order(cls, order_items: List[OrderItem]) -> Order:
        with DomainEvents() as domain_events:
            # Create callbacks for 'side effects' that are related to domain logic,
            # and which should be handled by the Application Layer
            callbacks = [
                DomainEventCallable(OrderEvent.HIGH_VOLUME_PRICE, cls.notify_top_management),
                DomainEventCallable(OrderEvent.HIGH_VOLUME_PRICE, cls.notify_sales_team),
                DomainEventCallable(OrderEvent.HIGH_QUANTITY, cls.notify_inventory_team)
            ]

            # Register for these domain events
            for callback in callbacks:
                domain_events.register_event(callback)

            order = Order()

            order.add_order_items(order_items)

            return order

    @classmethod
    def create_order_without_domain_events(cls, order_items: List[OrderItem]) -> Order:
        order = Order()

        order.add_order_items(order_items)

        return order

    @staticmethod
    def notify_sales_team(order: Order) -> None:
        """A callback for notifying the sales team about the important order"""

    @staticmethod
    def notify_top_management(order: Order) -> None:
        """A callback for notifying the top management about the important order"""

    @staticmethod
    def notify_inventory_team(order: Order) -> None:
        """A callback for notifying the inventory team required quantities"""


class DomainEventsTests(unittest.TestCase):
    def test_create_order_should_receive_domain_events_given_registered_to_domain_events(self):
        with patch.object(OrderService, 'notify_sales_team') as notify_sales_team, \
                patch.object(OrderService, 'notify_top_management') as notify_top_management, \
                patch.object(OrderService, 'notify_inventory_team') as notify_inventory_team:
            # given
            order_items = [OrderItem(product_id='1', price=100, quantity=10_000)]

            # when
            result = OrderService.create_order(order_items)

            # then
            notify_sales_team.assert_called_with(order=result)
            notify_top_management.assert_called_with(order=result)
            notify_inventory_team.assert_called_with(order=result)

    def test_create_order_should_not_receive_domain_events_given_not_registered_to_domain_events(self):
        with patch.object(OrderService, 'notify_sales_team') as notify_sales_team, \
                patch.object(OrderService, 'notify_top_management') as notify_top_management, \
                patch.object(OrderService, 'notify_inventory_team') as notify_inventory_team:
            # given
            order_items = [OrderItem(product_id='1', price=100, quantity=10_000)]

            # when
            result = OrderService.create_order_without_domain_events(order_items)

            # then
            notify_sales_team.assert_not_called()
            notify_top_management.assert_not_called()
            notify_inventory_team.assert_not_called()

    def test_register_event_should_throw_given_no_domain_event_context(self):
        # given
        domain_events = DomainEvents()

        # when & then
        with self.assertRaises(MissingDomainEventsContext):
            domain_events.register_event(DomainEventCallable(OrderEvent, MagicMock()))
