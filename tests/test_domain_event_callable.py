import unittest
from enum import Enum
from unittest.mock import MagicMock

from ddd_domain_events import DomainEventCallable


class EventType(Enum):
    EVENT_1 = 1
    EVENT_2 = 2
    

class DomainEventTests(unittest.TestCase):
    def test_call(self):
        # given
        callback = MagicMock()
        callable = DomainEventCallable(EventType.EVENT_1, callback)
        
        # when
        callable('some value', key='val')
        
        # then
        callback.assert_called_with('some value', key='val')
        self.assertEqual(EventType.EVENT_1, callable.event_type)

