__version__ = '0.1.6'

from .domain_events import DomainEvents
from .domain_event_callable import DomainEventCallable
